// định nghĩa
// hàm ko có tham số
function sayHello() {
  console.log("hello");
  console.log("hello");
  console.log("hello");
}

// thực thi ~ sử dụng ~ gọi hàm
sayHello();
sayHello();
sayHello();

// hàm có tham số
function sayHelloByName(username) {
  console.log("Hello ", username);
}

sayHelloByName("Alice");
sayHelloByName("Bob");

function introduce(name, age) {
  console.log(`Tôi là : ${name} - ${age} : tuổi`);
}
// introduce(2, "alice");
introduce("alice", 2);

function tinhDTB(toan, van) {
  var dtb = (toan + van) / 2;
  //   console.log(" dtb", dtb);
  return;
  console.log("hello");
}
var dtb1 = tinhDTB(8, 9);
console.log("dtb1", dtb1);
var dtb2 = tinhDTB(5, 9);
console.log("dtb2", dtb2);

// ko tham số, có tham số, giá trị trả về (return)

// return => trả về giá trị và dừng function tại đó
