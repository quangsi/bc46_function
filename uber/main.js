const UBER_CAR = "uberCar";
const UBER_SUV = "uberSUV";
const UBER_BLACK = "uberBlack";

// nhận vào 1 loại xe và trả về số tiền km đầu tiên theo từng loại xe
function tinhGiaTienKmDauTien(loaiXe) {
  if (loaiXe == UBER_CAR) {
    return 8000;
  }
  if (loaiXe == UBER_SUV) {
    return 9000;
  }
  if (loaiXe == UBER_BLACK) {
    return 10000;
  }
  //   return ketQua;
}

function tinhGiaTienKm1Den19(loaiXe) {
  if (loaiXe == UBER_CAR) {
    return 7500;
  }
  if (loaiXe == UBER_SUV) {
    return 8500;
  }
  if (loaiXe == UBER_BLACK) {
    return 9500;
  }

  //   3 if = if else if eles
}
function tinhGiaTienKm19TroDi(loaiXe) {
  // swith case
  switch (loaiXe) {
    case UBER_CAR: {
      return 7000;
    }
    case UBER_SUV: {
      return 8000;
    }
    case UBER_BLACK: {
      return 9000;
    }
  }
}

// main function : đảm nhiệm chứng năng chính
function tinhTienUber() {
  // lấy giá trị
  var loaiXe = document.querySelector('input[name="selector"]:checked').value;
  var soKm = document.getElementById("txt-km").value * 1;
  //   tính giá tiền theo km của từng loại xe
  var giaTienKmDauTien = tinhGiaTienKmDauTien(loaiXe);
  var giaTienKm1Den19 = tinhGiaTienKm1Den19(loaiXe);
  var giaTiemKm19TroDi = tinhGiaTienKm19TroDi(loaiXe);
  console.log({
    giaTienKmDauTien,
    giaTienKm1Den19,
    giaTiemKm19TroDi,
    soKm,
  });
  //   áp dụng công thức để tính tiền phải tiền
  var tongTien = 0;
  if (soKm <= 1) {
    tongTien = giaTienKmDauTien * soKm;
  } else if (soKm <= 19) {
    tongTien = giaTienKmDauTien + (soKm - 1) * giaTienKm1Den19;
  } else {
    tongTien =
      giaTienKmDauTien + 18 * giaTienKm1Den19 + (soKm - 19) * giaTiemKm19TroDi;
  }
  // hiển thị số tiền cần phải trả
  document.getElementById("divThanhTien").style.display = "block";
  document.getElementById("xuatTien").innerText = tongTien;
}

// SUV 20km
// 170.000
